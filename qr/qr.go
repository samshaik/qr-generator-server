package qrlogic

import (
	"encoding/base64"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/skip2/go-qrcode"
)

// GenerateQRCodeHandler generates a QR code for the provided URL
func GenerateQRCodeHandler(w http.ResponseWriter, r *http.Request) {
	urlParam := r.URL.Query().Get("url")

	if urlParam == "" {
		http.Error(w, "URL parameter is missing", http.StatusBadRequest)
		return
	}

	sizeParam := r.URL.Query().Get("size")
	size := 256 // Default size
	if sizeParam != "" {
		parsedSize, err := strconv.Atoi(sizeParam)
		if err == nil {
			size = parsedSize
		}
	}

	qr, err := qrcode.New(urlParam, qrcode.Medium)
	if err != nil {
		http.Error(w, "Failed to generate QR code", http.StatusInternalServerError)
		return
	}

	qrBytes, err := qr.PNG(size)
	if err != nil {
		http.Error(w, "Failed to generate QR code image", http.StatusInternalServerError)
		return
	}
    
	qrBase64 := base64.StdEncoding.EncodeToString(qrBytes)
	w.Header().Set("Content-Type", "application/json")
	response := map[string]string{"imageBase64": qrBase64}
	json.NewEncoder(w).Encode(response)

}

package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"qr-code-generator/qr"
	"github.com/rs/cors"
)

func main() {
    mux := http.NewServeMux()

    mux.HandleFunc("/generate", qrlogic.GenerateQRCodeHandler)

    corsHandler := cors.AllowAll().Handler(mux)

    port := os.Getenv("PORT")
    if port == "" {
        port = "8080"
    }
	addr := fmt.Sprintf(":%s", port)
	
    fmt.Printf("QR Code Generator is running on port %s...\n", port)
    if err := http.ListenAndServe(addr, corsHandler); err != nil {
        log.Fatalf("Failed to start server: %v", err)
    }
}
